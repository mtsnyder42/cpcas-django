from django.contrib import admin
from .models import AnimalControlContact, ResourceContact

admin.site.register(AnimalControlContact)
admin.site.register(ResourceContact)
