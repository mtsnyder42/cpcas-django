from __future__ import unicode_literals

from django.db import models

class ResourceContact(models.Model):
    name = models.CharField(max_length=64)
    doctors_name = models.CharField(max_length=32, null=True, blank=True)
    address = models.CharField(max_length=64)
    city = models.CharField(max_length=64)
    state = models.CharField(max_length=2)
    zip_code = models.IntegerField()
    po_box = models.CharField(max_length=24, null=True, blank=True)
    phone_number = models.IntegerField()
    fax_number = models.IntegerField(null=True, blank=True)
    
    def __str__(self):
        return self.name

class AnimalControlContact(models.Model):
    city = models.CharField(max_length=32)
    phone_number = models.IntegerField()
    fax_number = models.IntegerField(null=True, blank=True)
    
    def __str__(self):
        return 'Animal Control - ' + self.city