from django.conf.urls import url
from django.views.generic import TemplateView
import views

urlpatterns = (
    url(r'^$', views.home, name='home'),
    url(r'^resources/$', views.resources, name='resources'),
    url(r'^adopt/$', views.adopt, name='adopt'),
    url(r'^help/$', views.volunteer, name='help'),
    url(r'^volunteer/$', views.volunteer, name='volunteer'),
    url(r'^donate/$', views.volunteer, name='donate'),
    url(r'^wishlist/$', views.wishlist, name='help'),
)
