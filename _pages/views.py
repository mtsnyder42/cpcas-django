from django.shortcuts import render
from datetime import datetime    
from news_events.models import EventPost
from models import AnimalControlContact, ResourceContact

def home(request):
	return render(request, 'static/home.html', { 
			'featured_image' : '/static/images/banner.jpg', 
			'news_events' : EventPost.objects.all().filter(expiration_date__gte=datetime.now()).order_by('event_date'),
	})

def resources(request):
	return render(request, 'static/resources.html', { 
		'animal_control_contacts' : AnimalControlContact.objects.all(), 
		'resource_contacts' : ResourceContact.objects.all().order_by('city') 
	})

def adopt(request):
	return render(request, 'static/adopt.html', '')

def volunteer(request):
	return render(request, 'static/partial/volunteer.html', '')

def donate(request):
	return render(request, 'static/partial/donate.html', '')
	
def wishlist(request):
	return render(request, 'static/partial/wishlist.html', '')
