from __future__ import unicode_literals

from django.db import models
from django.forms import ModelForm, PasswordInput, TextInput
from django.contrib.auth.models import User

# this is used to create the form for the register model.
class RegisterForm(ModelForm):
	class Meta:
		model = User
		widgets = { 'password' : PasswordInput(attrs={'class' : 'form-control', 'placeholder' : 'Password'}),
			    'username' : TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Username'}),
			    'first_name' : TextInput(attrs={'class' : 'form-control', 'placeholder' : 'First Name' }),
			    'last_name' : TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Last Name' }),
			    'email' : TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Email Address'}), 
			}
		labels = { 'password' : '', 'username' : '', 'first_name' : '', 'last_name' : '', 'email' : '', }
		help_texts = { 'username': None, }
		fields = ['username', 'password', 'first_name', 'last_name', 'email']

# this is uneccessary, but it makes things more uniform
class LoginClass(models.Model):
	username = models.CharField(max_length=32)
	password = models.CharField(max_length=32)

class LoginForm(ModelForm):
	class Meta:
		model = LoginClass
		widgets = { 'password' : PasswordInput(attrs={'class' : 'form-control', 'placeholder' : 'Password'}),
			    'username' : TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Username' }),
			  }
		labels = { 'password' : '', 'username' : '' }
		fields = [ 'username', 'password' ]
