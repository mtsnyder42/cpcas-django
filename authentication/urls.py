from django.conf.urls import url
from django.views.generic import TemplateView
import views

urlpatterns = (
    url(r'^login/$', views.log_in, name='log in'),
    url(r'^logout/$', views.log_out, name='log out'),
    url(r'^register/$', views.register, name='register')
)
