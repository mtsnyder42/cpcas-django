from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from models import RegisterForm, LoginForm, LoginClass

def log_in(request):
    login_form = LoginForm()
    if request.user.is_authenticated():
        return redirect('/')
    if request.method == 'POST':
        login_request = LoginForm(request.POST)
        if login_request.is_valid():
            login_model = login_request.save(commit=False)
            user = authenticate(username=login_model.username, password=login_model.password)
            if user is not None:
                login(request, user)
                return redirect(request.POST['destination_url'])
            else:
                return render(request, 'static/login.html', { 'login_form' : login_form, 'login_error' : 'Invalid Credentials' })
    else:
        return render(request, 'static/login.html', { 'login_form' : login_form, 'source_url' : request.META.get('HTTP_REFERER') })
    
def log_out(request):
	logout(request)
	return redirect('/')
	
def register(request):
    register_form = RegisterForm()
    if request.method == "POST":
        new_registration = RegisterForm(request.POST)
        if new_registration.is_valid():
            new_user = User.objects.create_user(new_registration.cleaned_data['username'], email=new_registration.cleaned_data['email'], password=new_registration.cleaned_data['password'])
            new_user.first_name = new_registration.cleaned_data['first_name']
            new_user.last_name = new_registration.cleaned_data['last_name']
            new_user.save()
            new_user = authenticate(username=new_registration.cleaned_data['username'], password=new_registration.cleaned_data['password'])
            login(request, new_user)
            return redirect('/')
        else:
            return render(request, 'static/register.html', { 'register_form' : register_form, 'register_error' : new_registration.errors })
    return render(request, 'static/register.html', { 'register_form' : register_form })