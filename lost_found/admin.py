from django.contrib import admin

from .models import Pet, Size, Color, CoatLength, Species, Gender, PostType, Breed, Collar, Tags, Fixed

admin.site.register(Pet)
admin.site.register(Size)
admin.site.register(Color)
admin.site.register(Species)
admin.site.register(CoatLength)
admin.site.register(Breed)
admin.site.register(Collar)
admin.site.register(Tags)

admin.site.register(Gender)
admin.site.register(PostType)
admin.site.register(Fixed)
