from __future__ import unicode_literals

from django.apps import AppConfig


class LostFoundConfig(AppConfig):
    name = 'lost_found'
    verbose_name = 'Lost & Found'
