# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-25 01:29
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('lost_found', '0007_auto_20170325_0034'),
    ]

    operations = [
        migrations.CreateModel(
            name='PostType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.IntegerField(choices=[(1, 'Lost'), (2, 'Found')])),
            ],
        ),
        migrations.AddField(
            model_name='pet',
            name='type',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='lost_found.PostType'),
            preserve_default=False,
        ),
    ]
