# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-30 21:07
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lost_found', '0016_auto_20170330_0135'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Fur',
            new_name='CoatLength',
        ),
        migrations.RenameField(
            model_name='pet',
            old_name='fur',
            new_name='coat_length',
        ),
    ]
