from __future__ import unicode_literals
import os

from django import forms
from django.db import models
from django.forms import ModelForm, TextInput, ModelChoiceField, Select, FileInput
from django.contrib.auth.models import User

class Size(models.Model):
        value = models.CharField(max_length=16)
	def __str__(self):
		return self.value
	@property
	def name(self):
		return 'Size'

class Color(models.Model):
        value = models.CharField(max_length=16)
	def __str__(self):
		return self.value
	@property
	def name(self):
		return 'Color'

class CoatLength(models.Model):
        value = models.CharField(max_length=16)
	def __str__(self):
		return self.value
	@property
	def name(self):
		return 'Coat Length'

class Species(models.Model):
	value = models.CharField(max_length=16)
	def __str__(self):
		return self.value
	@property
	def name(self):
		return 'Species'
		
class Tags(models.Model):
	value = models.CharField(max_length=16)
	def __str__(self):
		return self.value
	@property
	def name(self):
		return 'Tags'
		
class Collar(models.Model):
	value = models.CharField(max_length=16)
	def __str__(self):
		return self.value
	@property
	def name(self):
		return 'Collar'

class Breed(models.Model):
	value = models.CharField(max_length=32)
	def __str__(self):
		return self.value
	@property
	def name(self):
		return 'Breed'
	
class Gender(models.Model):
	value = models.IntegerField(choices=((1, 'Male'), (2, 'Female')))
	def __str__(self):
		if self.value == 1:
			return 'Male'
		return 'Female'
	@property
	def name(self):
		return 'Gender'

### EXAMPLE FOR HOW TO ADD MORE ATTRIBUTES ###		
class ExampleAttribute(models.Model):
	value = models.CharField(max_length=32)
	def __str__(self):
		return self.value
	@property
	def name(self):
		return 'ExampleAttribute'
		
class Fixed(models.Model):
	value = models.IntegerField(choices=((1, 'Spayed'), (2, 'Neutered'), (3, 'No'), (4, 'Unknown')))
	def __str__(self):
		if self.value == 1:
			return 'Spayed'
		if self.value == 2:
			return 'Neutered'
		if self.value == 3:
			return 'No'
		else:
			return 'Unknown'
	@property
	def name(self):
		return 'Spayed/Neutered'

class PostType(models.Model):
	value = models.IntegerField(choices=((1, 'Lost'), (2, 'Found')))
	def __str__(self):
		if self.value == 1:
			return 'Lost'
		return 'Found'

class Pet(models.Model):
	type = models.ForeignKey(PostType)
	name = models.CharField(max_length=32, null=True, blank=True)
	contact_number = models.IntegerField(max_length=10)
	size = models.ForeignKey(Size)
	color = models.ForeignKey(Color)
	gender = models.ForeignKey(Gender)
	coat_length = models.ForeignKey(CoatLength)
	species = models.ForeignKey(Species)
	breed = models.ForeignKey(Breed)
	collar = models.ForeignKey(Collar)
	tags = models.ForeignKey(Tags)
	spayed_or_neutered = models.ForeignKey(Fixed)
	missing_from = models.CharField(max_length=64, null=True, blank=True)
	unique_characteristics = models.CharField(max_length=64, null=True, blank=True)
	photo = models.ImageField(null=True, blank=True)
	posted = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	creator = models.ForeignKey(User)
	missing_since = models.DateField()
	
	def __str__(self):
		return self.name
		
	def get_fields(self):
		return [(field.name, field.value_to_string(self)) for field in Pet._meta.fields]
		
class DateInput(forms.DateInput):
    input_type = 'date'

# this is used to generate the form for posting
class PetPostForm(ModelForm):
	def __init__(self, *args, **kwargs):
		super(PetPostForm, self).__init__(*args, **kwargs)
		self.fields['size'].empty_label = 'Select a size...'
		self.fields['color'].empty_label = 'Select a color...'
		self.fields['gender'].empty_label = 'Select a gender...'
		self.fields['coat_length'].empty_label = 'Select a coat length...'
		self.fields['species'].empty_label = 'Select a species...'
		self.fields['type'].empty_label = 'Select a type...'
		self.fields['breed'].empty_label = 'Select a breed...'
		self.fields['collar'].empty_label = 'Select type of collar....'
		self.fields['tags'].empty_label = 'Select type of tags...'
		self.fields['spayed_or_neutered'].empty_label = 'Spayed or Neutered?'
		self.fields['creator'].required = False
        
	class Meta:
		model = Pet
		fields = [ 'type', 'name', 'size', 'color', 'gender', 'coat_length', 'species', 'spayed_or_neutered', 'creator', 'breed', 'collar', 'tags', 'missing_since', 'missing_from', 'unique_characteristics', 'contact_number', 'photo', ]
		widgets = { 'name' : TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Pet Name (if known)'}),
			    'size' : Select(attrs={'class' : 'form-control' }),
			    'color' : Select(attrs={'class' : 'form-control' }),
			    'gender' : Select(attrs={'class' : 'form-control' }),
			    'coat_length' : Select(attrs={'class' : 'form-control' }),
			    'breed' : Select(attrs={'class' : 'form-control' }),
			    'collar' : Select(attrs={'class' : 'form-control' }),
			    'tags' : Select(attrs={'class' : 'form-control' }),
			    'species' : Select(attrs={'class' : 'form-control' }),
			    'spayed_or_neutered' : Select(attrs={'class' : 'form-control' }),
			    'unique_characteristics' : TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Unique Characteristics...'}),
			    'missing_from' : TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Missing/Found From...'}),
			    'missing_since' : TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Missing Since/Found On...', 'onfocus': '(this.type="date")'}),
			    'type' : Select(attrs={'class' : 'form-control' }),
			    'photo' : FileInput(attrs={'class' : 'form-control btn btn-info', 'accept' : 'image/*', }),
			    'contact_number' : TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Enter a contact number...'}),
			    'creator' : TextInput(attrs={'style' : 'display:none; '}),
		          }
		labels = { 'unique_characteristics' : '', 
		'name' : '', 
		'size' : '', 
		'color' : '', 
		'gender' : '', 
		'coat_length' : '', 
		'species' : '', 
		'type' : '', 
		'photo' : 'Upload Photo', 
		'creator' : '', 
		'collar' : '', 
		'breed' : '', 
		'tags' : '', 
		'missing_from' : '', 
		'missing_since' : '', 
		'spayed_or_neutered' : '', 
		'contact_number' : ''}