/*global $*/
$(document).ready(function() {
	
   $('#lost-found-filters').on('click', '.lost-found-filter-unselected', function() {
		toggleFilter($(this), false);
   });

   $('#lost-found-filters').on('click', '.lost-found-filter-selected', function() {
        toggleFilter($(this), true);
   });
   
   	$('#filter-mobile-collapse').click(function() { 
   		$(this).find('i').toggleClass('glyphicon-chevron-up glyphicon-chevron-down');	
   	});
   	
});

var filtersApplied = 0;

function toggleFilter(filterItem, isOn) {

	filterItem.toggleClass('lost-found-filter-selected lost-found-filter-unselected');
	filterItem.find('i').toggleClass('glyphicon-plus glyphicon-minus');

	if (isOn) 
		filtersApplied--;
	else
		filtersApplied++;

	// show all
	$('.lost-found-pets').show();

	var filterList  = { };
		
	$.each($('.lost-found-filter-title'), function(i, filterType) {
		var appliedFilters = [];

		$.each($('.lost-found-filter-selected'), function(i, selectedFilter) {
			if ($(selectedFilter).attr('filtertype') == $(filterType).html())
				appliedFilters.push($(selectedFilter).attr('filtervalue'));
		});

		filterList[$(filterType).html()] = appliedFilters;
	});

	$.each($('span.filterable'), function(i, petInfo) {
		for (var filterType in filterList) {
			if (filterList[filterType].length == 0)
				continue;
			if (filterType == $(petInfo).attr('filtertype')) {
				for (var filterValue in filterList[filterType]) {
					if (!filterList[filterType].includes($(petInfo).attr('filtervalue'))) {	
						$(petInfo).closest('.lost-found-pets').hide();
					}
				}
			}
		}
	});
	
	// no filters applied
	if (filtersApplied == 0)
		$('.lost-found-pets').show();
}
