from django.conf.urls import url
from django.views.generic import TemplateView
import views

urlpatterns = (
    url(r'^lost-found/$', views.lost_found, name='lost & found'),
    url(r'^lost-found/missing/$', views.lost_found, name = 'lost & found - missing'),
    url(r'^lost-found/found/$', views.found, name='lost & found - found'),
    url(r'^lost-found/post/$', views.post, name='lost & found - post')
)
