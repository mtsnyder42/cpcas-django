from django.shortcuts import render
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.forms import modelformset_factory
from django.contrib.auth import logout
from django.shortcuts import redirect


from .models import Pet, PetPostForm, Size, Color, CoatLength, Gender, Species, Tags, Collar, Fixed, Breed

def lost_found(request):
	context = { 
	'pet_type' : 1,
	'lost_found_pets' : Pet.objects.filter(type=1), 
	'filter_types' : {
				Size.objects.first().name : Size.objects.all(),
				Color.objects.first().name : Color.objects.all(),
				CoatLength.objects.first().name	: CoatLength.objects.all(),
				Gender.objects.first().name : Gender.objects.all(),
				Species.objects.first().name : Species.objects.all(),
				Fixed.objects.first().name : Fixed.objects.all(),
				Collar.objects.first().name : Collar.objects.all(),
				Breed.objects.first().name : Breed.objects.all(),
				Tags.objects.first().name : Tags.objects.all(),
			 },
	  }
	context['filter_keys'] = list(context['filter_types'].keys())
	return render(request, 'static/lost_found_pets.html', context)

def found(request):
	context = { 
	'pet_type' : 2,
	'lost_found_pets' : Pet.objects.filter(type=2), 
	'filter_types' : {
				Size.objects.first().name : Size.objects.all(),
				Color.objects.first().name : Color.objects.all(),
				CoatLength.objects.first().name	: CoatLength.objects.all(),
				Gender.objects.first().name : Gender.objects.all(),
				Species.objects.first().name : Species.objects.all(),
				Fixed.objects.first().name : Fixed.objects.all(),
				Collar.objects.first().name : Collar.objects.all(),
				Breed.objects.first().name : Breed.objects.all(),
				Tags.objects.first().name : Tags.objects.all(),
			 },
	  }
	context['filter_keys'] = list(context['filter_types'].keys())
	return render(request, 'static/lost_found_pets.html', context)
	
def post(request):
	pet_form = PetPostForm()
	if request.method == 'POST':
		if request.user.is_authenticated():
			request.POST['creator'] = request.user.id
			post_form = PetPostForm(request.POST, request.FILES)
			if post_form.is_valid():
				post_form.save()
				return render(request, 'static/post.html', {  'pet_form' : pet_form, 'alert_info' : 'Post submitted successfully!' })
			else:
			    return render(request, 'static/post.html', {  'pet_form' : pet_form, 'post_errors' : post_form.errors })

	return render(request, 'static/post.html', { 'pet_form' : pet_form })
