from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

class EventPost(models.Model):
    author = models.ForeignKey(User)
    title = models.CharField(max_length=32)
    content = models.TextField(max_length=5000)
    photo = models.ImageField(null=True, blank=True)
    photo_location = models.IntegerField(choices=((1, 'Center'), (2, 'Left'), (3, 'Right')), null=True, blank=True)
    event_date = models.DateTimeField()
    expiration_date = models.DateField()
    
    def __str__(self):
        return self.title
