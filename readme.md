# About
**This is the repository for the CPCAS Website**  
`_pages` - *the app that contains the static pages & templates*  
`lost_found` - *the app that contains the functionality for lost and found pets*  
    
# To Setup (Ubuntu)
**Install python package manager**  
`sudo apt-get install python-pip`  
**Install python virtualenv**  
`sudo apt-get install python-virtualenv`  
**Install requirements for Pillow**  
`sudo apt-get install libjpeg8-dev zlib1g-dev`  
**Setup Virtual Environment**  
`virtualenv CPCAS`    
**Install the dependencies**  
`sudo pip install -r requirements.txt`   
**Start the development webserver**  
`python manage.py runserver`  

The CPCAS Website should now be accessible on `localhost:8000`  
